package com.upv.practica_07;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class DisplayMessageActivity2 extends AppCompatActivity {
    String txtPhone;
    EditText txtMsg;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String numero = intent.getStringExtra(MainActivity.EXTRA_MESSAGE2);
        setContentView(R.layout.activity_display_message2);
        TextView tel =((TextView)findViewById(R.id.telefono));
        tel.setText(numero);
        txtPhone =numero;
        txtMsg = ((EditText)findViewById(R.id.txtMsg ));
        btnSend = ((Button)findViewById(R.id.btnSend ));
        btnSend.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                PendingIntent sentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("SMS_SENT"), 0);
                registerReceiver(new BroadcastReceiver() {
                                     @Override
                                     public void onReceive(Context context, Intent intent) {
                                         switch (getResultCode()){
                                             case Activity.RESULT_OK: Toast.makeText(getApplicationContext(), "SMS enviado", Toast.LENGTH_SHORT).show();
                                                 break;
                                             case SmsManager.RESULT_ERROR_GENERIC_FAILURE: Toast.makeText(getApplicationContext(), "No se pudo enviar SMS", Toast.LENGTH_SHORT).show();
                                                 break;
                                             case SmsManager.RESULT_ERROR_NO_SERVICE: Toast.makeText(getApplicationContext(), "Servicio no diponible", Toast.LENGTH_SHORT).show();
                                                 break;
                                             case SmsManager.RESULT_ERROR_NULL_PDU: Toast.makeText(getApplicationContext(), "PDU (Protocol Data Unit) es NULL", Toast.LENGTH_SHORT).show();
                                                 break;
                                             case SmsManager.RESULT_ERROR_RADIO_OFF: Toast.makeText(getApplicationContext(), "Failed because radio was explicitly turned off", Toast.LENGTH_SHORT).show();
                                                 break;
                                         }
                                     }
                                 },
                        new IntentFilter("SMS_SENT")); SmsManager sms = SmsManager.getDefault();
                if( txtPhone.toString().length()> 0 && txtMsg.getText().toString().length()>0 ) {
                    sms.sendTextMessage( txtPhone.toString() , null, txtMsg.getText().toString() , sentIntent, null);
                }
                else {
                    Toast.makeText(getApplicationContext(), "No se puede enviar, los datos son incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
