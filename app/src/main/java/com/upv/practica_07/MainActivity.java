package com.upv.practica_07;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import  android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {
    Button save,load;
    EditText message;
    String Message;
    public final static String EXTRA_MESSAGE1 = "com.upv.practica_07_1";
    public final static String EXTRA_MESSAGE2 = "com.upv.practica_07_2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public void llamar(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity1.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE1, message);
        startActivity(intent);
    }

    public void mensaje(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity2.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE2, message);
        startActivity(intent);
    }


}